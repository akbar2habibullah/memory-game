const icon = ['address-book', 'address-card', 'air-freshener', 'ambulance', 'anchor', 'angry', 'ankh', 'apple-alt', 'archive', 'archway']

const x = 6
const y = 6

const firstRandomIcon = []

for (let index = 0; index < (x * y / 2); index++) {
    firstRandomIcon.push(icon[Math.ceil(Math.random() * (icon.length)) - 1])
}

const secondRandomIcon = [...firstRandomIcon]
secondRandomIcon.sort()

const result = [...firstRandomIcon, ...secondRandomIcon]
    .map((a) => ({ id: (Math.ceil(Math.random() * 100000000)), value: `fa-${a}` }))
    .sort((a, b) => a.id - b.id)

export default result