import React, { useState } from 'react'

import MyContext from './context'
import RandomIcon from './random'

export default function Provider(props) {
    const [state, setState] = useState({
        icon: RandomIcon,
        clickedBefore: null,
    })

    const [score, setScore] = useState({
        value: 0
    })

    const [message, setMessage] = useState({
        value: "Let's play the game"
    })

    return (
        <MyContext.Provider value={{ state, setState, score, setScore, message, setMessage }}>
            {props.children}
        </MyContext.Provider>
    )
}
