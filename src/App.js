import React from 'react';
import './App.css';

import Provider from './provider'
import Base from './layout/base'
import Score from './components/score'
import Board from './components/board'

function App() {
  return (
    <Provider>
      <Base>
        <Score />
        <Board />
      </Base>
    </Provider>
  );
}

export default App;
