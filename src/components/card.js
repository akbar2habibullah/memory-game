import React from 'react'

import useWindowDimensions from '../window'

export default function Card({ icon, id, ref, func }) {
    const { width } = useWindowDimensions()

    return (
        <div className='card m-1'>
            <div className='card-body px-0'>
                <i className={`fas ${width > 768 ? 'fa-2x' : ''} ${icon}`} data-id={id} onClick={func}></i>
            </div>
        </div>
    )
}
