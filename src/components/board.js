import React from 'react'

import MyContext from '../context'
import Card from './card'

export default function Board() {
    return (
        <MyContext.Consumer>
            {context => (
                <>
                    <hr />
                    <div className='grid-container'>
                        {context.state.icon.map(a => <Card icon={a.value} id={a.id} func={(e) => {
                            e.target.classList.add('clicked')

                            if (context.state.clickedBefore !== null && context.state.clickedBefore.getAttribute('class') === e.target.getAttribute('class') && context.state.clickedBefore.getAttribute('data-id') !== e.target.getAttribute('data-id')) {
                                context.state.clickedBefore.classList.add('score')
                                e.target.classList.add('score')
                                context.setScore({
                                    value: context.score.value + 1
                                })
                                context.setMessage({
                                    value: 'You found the same cards!'
                                })
                                setTimeout(() => {
                                    context.setMessage({
                                        value: "Let's play the game"
                                    })
                                }, 2000)
                            } else if (context.state.clickedBefore !== null && context.state.clickedBefore.getAttribute('class') !== e.target.getAttribute('class')) {
                                context.state.clickedBefore.classList.remove('clicked')
                                e.target.classList.remove('clicked')

                                context.setMessage({
                                    value: 'You found the different cards!'
                                })

                                setTimeout(() => {
                                    context.setMessage({
                                        value: "Let's play the game"
                                    })
                                }, 2000)
                            }

                            if (context.state.clickedBefore === null) {
                                context.setState({
                                    ...context.state,
                                    clickedBefore: e.target
                                })
                            } else {
                                context.setState({
                                    ...context.state,
                                    clickedBefore: null
                                })
                            }

                        }} />
                        )}
                    </div>
                </>
            )}
        </MyContext.Consumer>
    )
}
