import React from 'react'

import MyContext from '../context'

export default function Score() {
    return (
        <MyContext.Consumer>
            {context => <h4 className='mx-3 text-left'>Score : {`${context.score.value}/${context.state.icon.length / 2}`}</h4>}
        </MyContext.Consumer>
    )
}
