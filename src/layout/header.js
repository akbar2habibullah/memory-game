import React from 'react'

export default function Header({ title, subtitle }) {
    return (
        <div className='mx-5'>
            <h1 className='mt-3'>{title}</h1>
            <h6>{subtitle}</h6>
        </div>
    )
}
