import React from 'react'

import Header from './header'
import Footer from './footer'

import Mycontext from '../context'

export default function Base(props) {
    return (
        <Mycontext.Consumer>
            {context => (
                <div className='text-center mx-auto'>
                    <Header title='Memory Game' subtitle='Made by Habibullah Akbar aka Chavyv Akvar' />
                    <p>{(context.score.value === (context.state.icon.length / 2)) ? 'Congrats! You win the game!' : context.message.value}</p>
                    <div className='cover mx-auto'>
                        <div className='mb-5'>
                            {props.children}
                        </div>
                    </div>
                    <Footer />
                </div>
            )}
        </Mycontext.Consumer>
    )
}
