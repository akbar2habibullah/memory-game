import React from 'react'

export default function Footer() {
    return (
        <p>
            Built with <a href='https://reactjs.org/'>React</a>, source available on <a href='https://gitlab.com/akbar2habibullah/memory-game'>Gitlab</a><br />
            Copyright &copy; 2020 Chavyv Akvar. All Rights Reserved
        </p>
    )
}
